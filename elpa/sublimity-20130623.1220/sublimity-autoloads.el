;;; sublimity-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "sublimity" "sublimity.el" (20971 25540 344174
;;;;;;  643000))
;;; Generated autoloads from sublimity.el

(autoload 'sublimity-scroll "sublimity" "\


\(fn)" t nil)

(autoload 'sublimity-map "sublimity" "\


\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("sublimity-map.el" "sublimity-pkg.el"
;;;;;;  "sublimity-scroll.el") (20971 25540 362151 95000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; sublimity-autoloads.el ends here
