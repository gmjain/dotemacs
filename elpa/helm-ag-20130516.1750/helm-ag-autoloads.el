;;; helm-ag-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "helm-ag" "helm-ag.el" (20971 40626 169946
;;;;;;  956000))
;;; Generated autoloads from helm-ag.el

(autoload 'helm-ag-pop-stack "helm-ag" "\


\(fn)" t nil)

(autoload 'helm-ag-clear-stack "helm-ag" "\


\(fn)" t nil)

(autoload 'helm-ag-this-file "helm-ag" "\


\(fn)" t nil)

(autoload 'helm-ag "helm-ag" "\


\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; helm-ag-autoloads.el ends here
