;; init.el Loads everything
;; Last modified : Thu, 21 November 2013 02:51:29 EST

;;Emacs load path
(let* ((my-lisp-dir "~/.emacs.d/")
       (default-directory my-lisp-dir)
       (orig-load-path load-path))
  (setq load-path (cons my-lisp-dir nil))
  (normal-top-level-add-subdirs-to-load-path)
  (nconc load-path orig-load-path))

(add-to-list 'load-path "~/.emacs.d/plugins/simple-rtm/lisp")
(autoload 'simple-rtm-mode "simple-rtm"
  "Interactive mode for Remember The Milk" t)

;; CDK customizations
(require 'env-config)

;; Mode configurations
(require 'mode-config)

;;Custom functions and key bindings
(require 'utility-functions)

;;Custom keybindings
(require 'misc-bindings)


;;;;;;;;;;;;;;;;
;; ELPA Repos ;;
;;;;;;;;;;;;;;;;
(require 'package)
;; Any add to list for package-archives (to add marmalade or melpa) goes here
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")
                         ("tromey-weblogger" . "http://tromey.com/elpa")))
(package-initialize)


;;;;;;;;;;;;;;;;
;; Load Theme ;;
;;;;;;;;;;;;;;;;
(add-to-list 'custom-theme-load-path "~/.emacs.d/plugins/themes")
(add-to-list 'custom-theme-load-path "~/.emacs.d/plugins/themes/noctilux-theme")
(load-theme 'cdk-wombat t)


;;;;;;;;;;;;;;;;;;;;;;
;; Initialize Cedet ;;
;;;;;;;;;;;;;;;;;;;;;;
(require 'semantic/ia)
(require 'semantic/bovine/gcc)
(semantic-mode 1)
(global-semantic-idle-completions-mode t)
(global-semantic-decoration-mode t)
(global-semantic-highlight-func-mode t)
(global-semantic-show-unmatched-syntax-mode t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Initialize Xrefactory ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;
;(setq exec-path (cons "/home/hexdump/.emacs.d/plugins/xref" exec-path))
;(setq load-path (cons "/home/hexdump/.emacs.d/plugins/xref/emacs" load-path))
;(load "xrefactory")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ggtags-global-output-format (quote grep))
 '(weblogger-config-alist (quote (("default" "http://grvj.wordpress.com/xmlrpc.php" "grvj" "" "60489271")))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'upcase-region 'disabled nil)
